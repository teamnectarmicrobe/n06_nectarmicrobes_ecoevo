---
title: "N06-P07-E06 Site Comparison"
author: "Callie Chappell"
date: '2022-06-15'
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Introduction

This is the analysis of a large field survey collected at Jasper Ridge Biological Preserve at Jasper Ridge Biological Preserve on Thursday, May 26th by Callie (Part 1) and Saturday, May 28th, 2022 with Callie Chappell, Tadashi Fukami, and Mark Bitter (Part 2) and San Gregorio (along Stage Road) on Saturday, June 11th, 2022 with Lexi Golden and Callie Chappell.  

The data was input by Calli (JRBP preliminary data, San Gregorio data) and Tad's team of FWS students. 

Note that a pH of 0 means that the flower had no nectar or not enough nectar to drop across all 3 parts of the pH strip 

# Resources

Link to lab notebook entry describing the methodologies used: https://docs.google.com/document/d/1RBQtQP6hjGs3gWCdDjnbyGYUADJrmW_6RDlEp073OcY/edit 

Link to raw data and metadata (downloaded as csv): https://docs.google.com/spreadsheets/d/1GtpXn7VX53g7GliiJEaS69PT9QD-5dA_-zjHDzqJls8/edit?usp=sharing

Link to Jasper Ridge map: https://www.google.com/maps/d/edit?mid=1H8xWsuzopnxXtfTYgPn-OF6dfQobc2g&usp=sharing 

Link to San Gregorio map: 

# Set up

Load packages
```{r, echo=FALSE, warning=FALSE, results='hide'}
#data organization and visualization: 
library(readr)
library(ggplot2)
library(knitr)
library(dplyr)
library(tidyr)
library(plotly)
library(readxl)

#statistical analysis: 
library(mixtools)
library(diptest)

library(glmmTMB)
library(DHARMa)
library(emmeans)
```

Initialize plot color scheme: 
```{r}
stigma_colors <- c("open" = "gray0", "closed" = "gray50", "NA" = "gray100")
anther_colors <- c("1" = "goldenrod1", "2" = "darkgoldenrod3", "3" = "darkorange", "4" = "darkorange4")

# true_site_colors <- c("BB" = "#aad2e2", 
#                       "MW" = "#fbc881", 
#                       "SB" = "#cfbcda", 
#                       "SG" = "#8762b0", 
#                       "JR" = "#f6b8b7", 
#                       "LH" = "#e65155", 
#                       "SA" = "#f59e50", 
#                       "SV" = "#b96527", 
#                       "OH" = "#373838", 
#                       "JP" = "#5bb870", 
#                       "CH" = "#c6e1a4", 
#                       "SR" = "#edd34b",  
#                       "BS" = "#2a88c2")


true_site_colors <- c("SG" = "#8762b0", 
                      "JR" = "#f6b8b7")

community_colors <- c("Fungi-dominated" = "#FBD848", "Bacteria-dominated" = "#0f0e61", "Co-dominated"="seagreen", "Too_rare"="grey95", "No_microbes" = "grey71")

nectar_fill_colors <- c("nectar"="grey50", "no_nectar"="white")
nectar_color_colors <- c("nectar"="black", "no_nectar"="black")

plate_shapes <- c("tsa_growth"=19, "no_tsa_growth"=1)

culture_colors <- c("black"="black", "grey"="grey50", "white"="grey90")
```

Set global plotting settings: 
```{r}
# theme_set(
#     theme_classic(base_size = 8, base_family = "Calibri")
# )

theme_set(
    theme_classic(base_size = 12)
)

#extrafont::font_import(quiet=TRUE)

```

# Load data

Import survey data: 
```{r}
df <- read_csv("N06-P07-E05 San Gregorio and JRBP - Sheet1.csv")

#make anther as factor: 
df$anther <- as.factor(df$anther)

#make flower unique number as factor: 
df$flower_unique <- as.factor(df$flower_unique)

#make subsite unique number as factor: 
df$subsite <- as.factor(df$subsite)

#remove column 17: 
df <- df[-c(17)]
```

Import Dish data: 
```{r}
df_dish <- read_csv("BSURP 2022 Dish data - Reformatted data for Callie.csv")
```


Make one dataframe with data from survey and Dish: 
```{r}
df_combined <- rbind(df, df_dish)
```
Make some minor modifications: 
```{r}
#make a category for nectar or not: 
df <- df %>% mutate(nectar=ifelse(pH>0, "nectar", "no_nectar"))
df_combined <- df_combined %>% mutate(nectar=ifelse(pH>0, "nectar", "no_nectar"))

# remove no nectar flowers; 
df_limited <- df %>% filter(pH>0)
df_all_limited <- df_combined %>% filter(pH>0)
```


Import culturing data: 
```{r}
df_culture <- read_csv("BSURP 2022 Dish data - Reformatted data for Callie - pH and culturing.csv")

#make a category for whether there is nectar or not 
df_culture <- df_culture %>% mutate(nectar=ifelse(pH>0, "nectar", "no_nectar"))

#renaming columns to remove spaces
names(df_culture)[26] <- "log_total_cfu_tsa"
df_culture$log_total_cfu_tsa <- as.numeric(df_culture$log_total_cfu_tsa)
names(df_culture)[29] <- "log_total_cfu_r2a"
df_culture$log_total_cfu_r2a <- as.numeric(df_culture$log_total_cfu_r2a)
names(df_culture)[27] <- "log_small_cfu_yma"
df_culture$log_small_cfu_yma <- as.numeric(df_culture$log_small_cfu_yma)
names(df_culture)[28] <- "log_large_cfu_yma"
df_culture$log_large_cfu_yma <- as.numeric(df_culture$log_large_cfu_yma)

#make df without zeros: 
df_culture_limited <- df_culture %>% dplyr::filter(pH!=0)
```




# Analysis 

## What is the range of *D. aurantiacus* nectar pH we observe in the field? 

**Across all flowers:**
```{r}
# hist_all <- ggplot(df, aes(x=pH)) +
#   geom_histogram(bindwidth=0.5) +
#   scale_x_continuous(breaks = seq(0,6,0.5), lim = c(0,6))

hist_all <- ggplot(df, aes(x=pH, fill=nectar, color=nectar)) +
  geom_bar(bindwidth=0.5) +
  scale_x_binned() +
  scale_fill_manual(values = nectar_fill_colors) +
  scale_color_manual(values = nectar_color_colors) +
  ylab("Number of flowers with this pH nectar")
  
ggplotly(hist_all)
```

Overall, it looks like we have a bimodal distribution. 

**Statistical test for bimodality:**

I'll be using Hartigan's Dip Test for unimodality to test for multimodality. 

Information about Dip Test: https://projecteuclid.org/journals/annals-of-statistics/volume-13/issue-1/The-Dip-Test-of-Unimodality/10.1214/aos/1176346577.full 
RDocumentatation: https://www.rdocumentation.org/packages/diptest/versions/0.76-0/topics/dip.test 
Notes on interpreting: https://stats.stackexchange.com/questions/156808/interpretation-of-hartigans-dip-test#:~:text=Hartigans'%20dip%20test%20is%20defined,that%20minimizes%20that%20maximum%20difference%22. 


```{r}
#make a vector of the pH only: 
vector_pH <- c(t(df_limited$pH))

#run dip test: 
dip.test(vector_pH)
```

Test results: D = 0.052083, p-value < 2.2e-16

This p-value suggests that we reject the null hypothesis that the data is unimodal. 

**Test for 2-part mixture model**

We will use an expectation-maximations (EM) algorithim to infer the value of the hidden groupings. The expectation-maximization algorithm is a popular iterative procedure that alternates between (1) pretending we know the probability with which each observation belongs to a component and estimating the parameters of the components,
and (2) pretending we know the parameters of the component distributions and estimating the probability with which each observation belongs to the components.

Note that this is a "soft-labeling" approach, which assigns a probability of each flower to being either in the "low pH" or "high pH" category. This differs from a clustering approach (like k-means) which assigns each datapoint (flower's pH) to a grouping. 

We're using the R package mixtools (https://www.r-bloggers.com/2011/08/fitting-mixture-distributions-with-the-r-package-mixtools/), which was suggested by Susan Holmes in her class BIOS 221 (lecture and lab #3). 

I also used this tutorial: https://tinyheero.github.io/2015/10/13/mixture-model.html

**K=2** 

In this model, I am assuming normal underlying distribution (gaussian). 
```{r}

#extract model outputs: 
#' @param x Input data
#' @param mu Mean of component
#' @param sigma Standard deviation of component
#' @param lam Mixture weight of component
plot_mix_comps <- function(x, mu, sigma, lam) {
  lam * dnorm(x, mu, sigma)}

set.seed(1)

#make just pH dataframe: 
pH_only <- df_limited$pH

#run model: 
mixmdl_k2 <- normalmixEM(pH_only, k=2)

summary(mixmdl_k2)
```
number of iterations= 111 
summary of normalmixEM object:
         comp 1   comp 2
lambda 0.571693 0.428307
mu     4.345910 7.616040
sigma  1.609583 0.769621
loglik at estimate:  -1178.624 

Plot: 
```{r}
#plot: 
hist_mix <- data.frame(x = mixmdl_k2$x) %>% ggplot()  +
  geom_histogram(aes(x, ..density..), binwidth = 1, colour = "black") +
  stat_function(geom = "line", fun = plot_mix_comps,
                args = list(mixmdl_k2$mu[1], mixmdl_k2$sigma[1], lam = mixmdl_k2$lambda[1]),
                colour = "black", lwd = 1.5) +
  stat_function(geom = "line", fun = plot_mix_comps,
                args = list(mixmdl_k2$mu[2], mixmdl_k2$sigma[2], lam = mixmdl_k2$lambda[2]),
                colour = "grey", lwd = 1.5) +
  ylab("Proportion of flowers with this pH nectar") +
  xlab("pH")
  
hist_mix
```


Summary statistics: 
```{r}
#what are the means of the two components? 
mixmdl_k2$mu

#what are the standard deviations of the two components? 
mixmdl_k2$sigma

#what are the relative heights of the two components? 
mixmdl_k2$lambda

#what is the probability of each flower's pH falling into one of the two groupings? 
#The x column indicates the value of the pH of each individual flower while comp.1 and comp.2 refers to the posterior probability of belonging to either component respectively.
post.df <- as.data.frame(cbind(x = mixmdl_k2$x, mixmdl_k2$posterior))
head(post.df) 

``` 


*What are the means of the two components?*
4.34591 7.61604

*What are the standard deviations of the two components?* 
1.609583 0.769621

*What are the relative heights of the two components?*  
0.5716927 0.4283073

*Final version of plot:*
```{r}

hist_all_dist <- ggplot(df_limited, aes(x=pH, fill=nectar, color=nectar)) +
  geom_histogram(binwidth=0.5, stat="count") +
  scale_fill_manual(values = nectar_fill_colors) +
  scale_color_manual(values = nectar_color_colors) +
  ylab("Number of flowers with this pH nectar") +
  mapply(function(mean, sd, lambda, n, binwidth) {
    stat_function(
      fun = function(x) {
        (dnorm(x, mean = mean, sd = sd)) * n * binwidth * lambda
        }
      )
    },
    mean = mixmdl[["mu"]], 
    sd = mixmdl[["sigma"]],
    lambda = mixmdl[["lambda"]],
    n = 728,
    binwidth = 0.5
  ) +
  theme(legend.position = "none")
  
hist_all_dist

ggsave("hist_all_dist.pdf", width=5, height=2.5)
```

**K=3:**

In this model, I am assuming normal underlying distribution (gaussian). 
```{r}
#extract model outputs: 
#' @param x Input data
#' @param mu Mean of component
#' @param sigma Standard deviation of component
#' @param lam Mixture weight of component
plot_mix_comps <- function(x, mu, sigma, lam) {
  lam * dnorm(x, mu, sigma)}

set.seed(1)

#make just pH dataframe: 
pH_only <- df_limited$pH

#run model: 
mixmdl <- normalmixEM(pH_only, k=3)

summary(mixmdl)
```

number of iterations= 126 
summary of normalmixEM object:
         comp 1   comp 2   comp 3
lambda 0.190189 0.334793 0.475018
mu     2.574893 7.841618 5.539768
sigma  0.513567 0.626182 1.272221
loglik at estimate:  -1142.255 

Plot: 
```{r}
#plot: 
hist_mix <- data.frame(x = mixmdl$x) %>% ggplot()  +
  geom_histogram(aes(x, ..density..), binwidth = 1, colour = "black") +
  stat_function(geom = "line", fun = plot_mix_comps,
                args = list(mixmdl$mu[1], mixmdl$sigma[1], lam = mixmdl$lambda[1]),
                colour = "#0f0e61", lwd = 1.5) +
  stat_function(geom = "line", fun = plot_mix_comps,
                args = list(mixmdl$mu[2], mixmdl$sigma[2], lam = mixmdl$lambda[2]),
                colour = "grey71", lwd = 1.5) +
  stat_function(geom = "line", fun = plot_mix_comps,
                args = list(mixmdl$mu[3], mixmdl$sigma[3], lam = mixmdl$lambda[3]),
                colour = "#FBD848", lwd = 1.5) +
  ylab("Proportion of flowers with this pH nectar") +
  xlab("pH")
  
hist_mix
```

This supports looks like a plausible trimodal distribution.  

Summary statistics: 
```{r}
#what are the means of the two components? 
mixmdl$mu

#what are the standard deviations of the two components? 
mixmdl$sigma

#what are the relative heights of the two components? 
mixmdl$lambda

#what is the probability of each flower's pH falling into one of the two groupings? 
#The x column indicates the value of the pH of each individual flower while comp.1 and comp.2 refers to the posterior probability of belonging to either component respectively.
post.df <- as.data.frame(cbind(x = mixmdl$x, mixmdl$posterior))
head(post.df) 

``` 

Means: 2.574893 7.841618 5.539768
Standard deviations: 0.5135673 0.6261825 1.2722211
Relative heights: 0.1901890 0.3347935 0.4750175

**Make final version of plot:**

```{r}

hist_all_dist <- ggplot(df_limited, aes(x=pH, fill=nectar, color=nectar)) +
  geom_histogram(binwidth=0.5, stat="count") +
  scale_fill_manual(values = nectar_fill_colors) +
  scale_color_manual(values = nectar_color_colors) +
  ylab("Number of flowers with this pH nectar") +
  mapply(function(mean, sd, lambda, n, binwidth) {
    stat_function(
      fun = function(x) {
        (dnorm(x, mean = mean, sd = sd)) * n * binwidth * lambda
        }
      )
    },
    mean = mixmdl[["mu"]], 
    sd = mixmdl[["sigma"]],
    lambda = mixmdl[["lambda"]],
    n = 728,
    binwidth = 0.5
  ) + geom_vline(xintercept = 2.574893, linetype="solid", 
                color = "#0f0e61", size=0.5) +
  geom_vline(xintercept = 2.32, linetype="dotted", 
                color = "#0f0e61", size=0.5) +
  geom_vline(xintercept = 7.841618, linetype="solid", 
                color = "grey71", size=0.5) +
  geom_vline(xintercept = 7.50, linetype="dotted", 
                color = "grey71", size=0.5) +
  geom_vline(xintercept = 5.539768, linetype="solid", 
                color = "#FBD848", size=0.5) +
  geom_vline(xintercept = 5.60, linetype="dotted", 
                color = "#FBD848", size=0.5) +
  ylim(0,80) +
  theme(legend.position = "none")
  
hist_all_dist

ggsave("hist_all_dist.pdf", width=5, height=2.5)

```


**Likelihood ratio test to compare k=2 vs. k=3**

Initialize all variables: 

ll = log likelihood (calculated above)
m is the number of parameters. Which again depends on the EM algorithm. If you are estimating both mean and variance for each component then m = 4 for k = 2 and m = 6 for k = 3 etc.
```{r}
ll_k2 <- -1178.624
ll_k3 <- -1142.255

m_k2 <- 4
m_k3 <- 6
```


Calculate LRT statistic (\lambda): 72.738

\lambda = -2 [ \ell_{k=2} - \ell_{k=3} ]

```{r}
lambda = -2*(ll_k2 - ll_k3)
lambda
```

\lambda is distributed as a chi-squared with degrees of freedom equal to the number of different parameters between k=2 & k=3. That might be 1 or 2 depending on whether your EM algorithm assumed equality of variances. You can get a p-value with the "pchisq(..., lower.tail = FALSE)" base R function. A significant p-value indicates that the additional parameter improves the fit significantly and would favor k=3 over k=2.

```{r}
pchisq(lambda, df = m_k3-m_k2, lower.tail = FALSE)
```
p=1.603777e-16 

Compare AIC values: 

AIC = 2m - 2\ell
```{r}
AIC_k2 <- 2*m_k2 - 2*ll_k2
AIC_k2

AIC_k3 <- 2*m_k3 - 2*ll_k3
AIC_k3
```
AIC_k2 = 2365.248
AIC_k3 = 2296.51

**Test across all sites and the Dish** 

**Across all flowers:**
```{r}
# hist_all <- ggplot(df, aes(x=pH)) +
#   geom_histogram(bindwidth=0.5) +
#   scale_x_continuous(breaks = seq(0,6,0.5), lim = c(0,6))

hist_all_survey_dish <- ggplot(df_combined, aes(x=pH, fill=nectar, color=nectar)) +
  geom_bar(bindwidth=1) +
  scale_x_binned() +
  scale_fill_manual(values = nectar_fill_colors) +
  scale_color_manual(values = nectar_color_colors) +
  ylab("Number of flowers with this pH nectar")
  
hist_all_survey_dish
```


### By site: 

```{r}
hist_site <- ggplot(df, aes(x=pH, fill=site)) +
  geom_bar(bindwidth=1) +
  scale_x_binned() +
  facet_grid(.~site) +
  scale_fill_manual(values = true_site_colors) +
  ylab("Number of flowers with this pH nectar")
  
hist_site
```
Color by anther: 
```{r}
hist_site_anther <- ggplot(df, aes(x=pH, fill=anther)) +
  geom_bar(bindwidth=1) +
  scale_x_binned() +
  facet_grid(.~site) +
  scale_fill_manual(values = anther_colors) +
  ylab("Number of flowers with this pH nectar")
  
hist_site_anther
```



Overlaid bar plots: 
```{r}
hist_site <- ggplot(NULL, aes(x=pH)) +
  geom_bar(aes(fill="SG"), data=df %>% filter(site=="SG"), alpha=1, bindwidth=1) +
  geom_bar(aes(fill="JR"), data=df %>% filter(site=="JR"), alpha=0.5, bindwidth=1) +
  scale_x_binned() +
  scale_fill_manual(values = true_site_colors) +
  ylab("Number of flowers with this pH nectar")
  
hist_site
```

If yeast lower the pH to an intermediate level and 


```{r}

hist_site_mix <- ggplot() +
  geom_bar(aes(x=pH, fill="SG"), data=df %>% filter(site=="SG"), alpha=1, bindwidth=1) +
  geom_bar(aes(x=pH, fill="JR"), data=df %>% filter(site=="JR"), alpha=0.5, bindwidth=1) +
  scale_x_binned() +
  scale_fill_manual(values = true_site_colors) + 
  stat_function(geom = "line", fun = plot_mix_comps,
                args = list(mixmdl$mu[1], mixmdl$sigma[1], lam = mixmdl$lambda[1]),
                colour = "#0f0e61", lwd = 1.5) +
  stat_function(geom = "line", fun = plot_mix_comps,
                args = list(mixmdl$mu[2], mixmdl$sigma[2], lam = mixmdl$lambda[2]),
                colour = "grey71", lwd = 1.5) +
  stat_function(geom = "line", fun = plot_mix_comps,
                args = list(mixmdl$mu[3], mixmdl$sigma[3], lam = mixmdl$lambda[3]),
                colour = "#FBD848", lwd = 1.5) +
  ylab("Number of flowers with this pH nectar")
  
hist_site_mix
```


### By flower trait (stigma vs. anther)

**Facet by stigma:**
```{r}
hist_stigma <- ggplot(filter(df_limited, stigma!="NA"), aes(x=pH, color=nectar, fill=nectar)) +
  geom_bar(bindwidth=0.5) +
  scale_fill_manual(values = nectar_fill_colors) +
  scale_color_manual(values = nectar_color_colors) +
  scale_x_binned() +
  facet_grid(site~forcats::fct_relevel(stigma, "open", "closed")) +
  ylab("Number of flowers with this pH nectar") + 
  theme(legend.position = "none") +
  ylim(0,30) +
  scale_x_continuous(name="Nectar pH", breaks=c(2,4, 6, 8))
  
hist_stigma


ggsave("hist_stigma.pdf", width=4, height=4)

hist_stigma_all <- ggplot(filter(df, stigma!="NA"), aes(x=pH, color=nectar, fill=nectar)) +
  geom_bar(bindwidth=0.5) +
  scale_fill_manual(values = nectar_fill_colors) +
  scale_color_manual(values = nectar_color_colors) +
  scale_x_binned() +
  facet_grid(site~stigma) +
  ylab("Number of flowers with this pH nectar") + 
  theme(legend.position = "none")

ggplotly(hist_stigma_all)

#count total number of flowers: 
df %>% count(site)

```


**Facet by age (anther status):** 
```{r}
hist_anther <- ggplot(filter(df_limited, anther!="NA"), aes(x=pH)) +
  geom_bar(bindwidth=0.5, aes(fill=anther)) +
  scale_fill_manual(values = anther_colors) +
  scale_x_binned() +
  facet_grid(site~anther) +
  ylab("Number of flowers with this pH nectar") + 
  theme(legend.position = "none") +
  ylim(0,60)

hist_anther

ggsave("hist_anther.pdf", width=5, height=3.5)
```

**Visualize raw pH for paired flowers:**
```{r}
pairs_dotplot <- ggplot(df, aes(x=pair_unique, y=pH, fill=site)) +
  geom_dotplot(binaxis='y', 
               stackdir='center', 
               dotsize = .5) +
  geom_boxplot(aes(alpha=0.01)) +
  xlab("Unique flower pair") +
  ylab("Flower nectar pH") +
  scale_fill_manual(values = true_site_colors)

pairs_dotplot
```


### Are flowers on the same plant more like to have similar nectar pH?

**Visualize nectar pH of flowers on the same plant**
```{r}
plant_boxplot <- ggplot(df, aes(x=plant_unique, y=pH, fill=site)) +
  geom_boxplot(aes(alpha=0.01)) +
  geom_dotplot(binaxis='y', 
               stackdir='center', 
               dotsize = .5) +
  scale_fill_manual(values = true_site_colors)

plant_boxplot
```

# Effect of plant traits on pH: 

**What is the effect of stigma status on nectar pH?** 

**Plots:** 

Effect of stigma status: 
```{r}
pH_stigma_boxplot <- ggplot(df, aes(x=stigma, y=pH)) +
  geom_boxplot() +
  geom_point(aes(alpha=.5), position=position_jitterdodge(jitter.height=0.1, jitter.width=.5), seed = 15) +
  ylab("Nectar pH") +
  xlab("Stigma Status") +
  theme(legend.position = "none")
  
pH_stigma_boxplot

pH_stigma_boxplot_site <- ggplot(df, aes(x=stigma, y=pH)) +
  geom_boxplot() +
  geom_point(aes(alpha=.5), position=position_jitterdodge(jitter.height=0.1, jitter.width=.5), seed = 15) +
  ylab("Nectar pH") +
  xlab("Stigma Status") +
  theme(legend.position = "none") +
  facet_grid(.~site)
  
pH_stigma_boxplot_site
```

Effect of anther status: 
```{r}
pH_anther_boxplot <- ggplot(df, aes(x=anther, y=pH, color=anther)) +
  geom_violin() +
  geom_point(aes(alpha=.5), position=position_jitterdodge(jitter.height=0.1, jitter.width=1.5), seed = 15) + 
  scale_color_manual(values = anther_colors) +
  ylab("Nectar pH") +
  xlab("Approximate age of flower (anther classification") +
  theme(legend.position = "none")
  
pH_anther_boxplot

pH_anther_boxplot_site <- ggplot(df, aes(x=anther, y=pH, color=anther)) +
  geom_boxplot() +
  geom_point(aes(alpha=.5), position=position_jitterdodge(jitter.height=0.1, jitter.width=1.5), seed = 15) + 
  scale_color_manual(values = anther_colors) +
  ylab("Nectar pH") +
  xlab("Approximate age of flower (anther classification") +
  theme(legend.position = "none") +
  facet_grid(.~site)
  
pH_anther_boxplot_site
```

Stigma by anther: 
```{r}
pH_anther_stigma_boxplot <- ggplot(df, aes(x=anther, y=pH, color=anther)) +
  geom_boxplot() +
  geom_point(aes(alpha=.5), position=position_jitterdodge(jitter.height=0.1, jitter.width=1.5), seed = 15) + 
  scale_color_manual(values = anther_colors) +
  ylab("Nectar pH") +
  xlab("Approximate age of flower (anther classification") +
  theme(legend.position = "none") +
  facet_grid(stigma~site)
  
pH_anther_stigma_boxplot


pH_stigma_anther_boxplot <- ggplot(df, aes(x=stigma, y=pH, color=anther)) +
  geom_boxplot() +
  geom_point(aes(alpha=.5), position=position_jitterdodge(jitter.height=0.1, jitter.width=1.5), seed = 15) + 
  scale_color_manual(values = anther_colors) +
  ylab("Nectar pH") +
  xlab("Approximate age of flower (anther classification") +
  theme(legend.position = "none") +
  facet_grid(anther~site)
  
pH_stigma_anther_boxplot

```

**Statistical tests: Comparing treatments** 

What is the effect of stigma status on nectar pH? 
```{r}
pH_stigma_test <- glmmTMB(pH ~ stigma*anther +
                           (1|site/subsite/plant_unique) +
                         (1|collected_by),
                   data=df) 

summary(pH_stigma_test)

##with lme4
```


Some quick diagnostics: (using the [DHARMa](https://cran.r-project.org/web/packages/DHARMa/vignettes/DHARMa.html) package)
```{r}
#calculate scaled residuals: 
pH_stigma_test_simres <- simulateResiduals(pH_stigma_test)
plot(pH_stigma_test_simres)
```



This doesn't pass the KS test, but it's not to snakey. 


*Pairwise analysis:*

Stigma alone: 
```{r}

pH_stigma_test_pairwise <- emmeans(pH_stigma_test, pairwise ~ stigma)
pairs(pH_stigma_test_pairwise)

```

p=0.0005, this suggests that there is a difference mean pH between different stigmas (closed stigma plants have lower pH)

Anther alone: 
```{r}
pH_anther_test_pairwise <- emmeans(pH_stigma_test, pairwise ~ anther)
pairs(pH_anther_test_pairwise)

```

 1 - 2       0.867 0.175 563   4.966  <.0001
 1 - 3       1.508 0.201 563   7.492  <.0001
 1 - 4       2.250 0.432 563   5.203  <.0001
 2 - 3       0.641 0.211 563   3.040  0.0132
 2 - 4       1.383 0.438 563   3.160  0.0090
 3 - 4       0.742 0.430 563   1.725  0.3113

This suggests that when together, all flowers differ between groups. 
 

Stigma by anther: 
```{r}

pH_stigma_anther_test_pairwise <- emmeans(pH_stigma_test, pairwise ~ stigma | anther)
pairs(pH_stigma_anther_test_pairwise)

#emmip(density_exp, evol_treatment ~ evol_treatment | experiment)
```

anther = 1:
 contrast      estimate    SE  df t.ratio p.value
 closed - open   -1.069 0.224 563  -4.762  <.0001

anther = 2:
 contrast      estimate    SE  df t.ratio p.value
 closed - open   -1.158 0.256 563  -4.516  <.0001

anther = 3:
 contrast      estimate    SE  df t.ratio p.value
 closed - open   -0.791 0.293 563  -2.702  0.0071

anther = 4:
 contrast      estimate    SE  df t.ratio p.value
 closed - open   -0.229 0.806 563  -0.284  0.7764

 
 This suggests that stigma status is  associated with different pH for all flowers except for very old ones (anther status 4). 
 
 **Statistical tests: Comparing sites** 

What is the effect of stigma status on nectar pH? 
```{r}
pH_stigma_test_site <- glmmTMB(pH ~ stigma*anther*site +
                           (1|subsite/plant_unique) +
                         (1|collected_by),
                   data=df) 

summary(pH_stigma_test_site)

##with lme4
```


Some quick diagnostics: (using the [DHARMa](https://cran.r-project.org/web/packages/DHARMa/vignettes/DHARMa.html) package)
```{r}
#calculate scaled residuals: 
pH_stigma_test_site_simres <- simulateResiduals(pH_stigma_test)
plot(pH_stigma_test_site_simres)
```

Again, this doesn't pass the KS test. 

*pairwise comparisons* 

Stigma by site: 
```{r}
pH_stigma_test_site_pairwise <- emmeans(pH_stigma_test_site, pairwise ~ stigma | site)
pairs(pH_stigma_test_site_pairwise)

#emmip(density_exp, evol_treatment ~ evol_treatment | experiment)
```


site = JR:
 contrast      estimate    SE  df t.ratio p.value
 closed - open   -0.820 0.457 556  -1.794  0.0734

site = SG:
 contrast      estimate    SE  df t.ratio p.value
 closed - open   -0.718 0.278 556  -2.581  0.0101
 
For Jasper Ridge, there was no difference in pH by stigma status, but for San Gregorio there is. 



Anther by site: 
```{r}
pH_anther_test_site_pairwise <- emmeans(pH_stigma_test_site, pairwise ~ anther | site)
pairs(pH_anther_test_site_pairwise)

#emmip(density_exp, evol_treatment ~ evol_treatment | experiment)
```

site = JR:
 contrast estimate    SE  df t.ratio p.value
 1 - 2       1.242 0.213 556   5.819  <.0001
 1 - 3       1.896 0.298 556   6.365  <.0001
 1 - 4       3.348 0.872 556   3.839  0.0008
 2 - 3       0.654 0.306 556   2.135  0.1434
 2 - 4       2.105 0.873 556   2.411  0.0762
 3 - 4       1.451 0.858 556   1.691  0.3294

site = SG:
 contrast estimate    SE  df t.ratio p.value
 1 - 2       0.301 0.292 556   1.033  0.7300
 1 - 3       1.134 0.277 556   4.091  0.0003
 1 - 4       1.644 0.501 556   3.278  0.0061
 2 - 3       0.833 0.293 556   2.848  0.0236
 2 - 4       1.342 0.513 556   2.616  0.0451
 3 - 4       0.509 0.494 556   1.031  0.7316
 
 For Jasper Ridge, anther status 1 differed from 2-4. For San Greogrio, 1 and 2 differed from 3 and 4. 
 
 # Linear regression: pH to CFU 
 We did a linear mixed model predicting pH from the logt10 total CFU/uL +1 on R2A and TSA and visualized using a scatterplot. 
 
 ## R2A: 

### All data: 

Plot: 
```{r}
#black points: 
pH_CFU_R2A_plot <- ggplot(df_culture_limited, aes(x= log_total_cfu_r2a, y= pH ))+
  geom_jitter(aes(alpha=0.75), height=0.2, show.legend = F) +
  ylab("Nectar pH") +
  xlab("Final density of bacteria on R2A (log(CFU/uL+1)")+
  geom_abline(intercept = 5.9153, slope = -0.9056)
pH_CFU_R2A_plot

#color by TSA CFU: 
pH_CFU_R2A_plot_colorramp <- ggplot(df_culture_limited, aes(x= log_total_cfu_r2a, y= pH, color=log_total_cfu_tsa ))+
  geom_jitter(height=0.2) +
  ylab("Nectar pH") +
  xlab("Final density of bacteria on R2A (log(CFU/uL+1)")+
  geom_abline(intercept = 5.9153, slope = -0.9056)
pH_CFU_R2A_plot_colorramp

#color if TSA growth:
df_culture_limited <- df_culture_limited %>% mutate(tsa_growth=ifelse(log_total_cfu_tsa>0, "tsa_growth", "no_tsa_growth"))
pH_CFU_R2A_plot_colorbinary <- ggplot(df_culture_limited, aes(x= log_total_cfu_r2a, y= pH, shape=tsa_growth ))+
  geom_jitter(height=0.2, alpha=0.75, show.legend = F) +
  ylab("Nectar pH") +
  xlab("Final density of bacteria on R2A (log(CFU/uL+1)")+
  geom_abline(intercept = 5.9153, slope = -0.9056) +
  scale_shape_manual(values = plate_shapes)
  
pH_CFU_R2A_plot_colorbinary
ggplotly(pH_CFU_R2A_plot_colorbinary)


ggsave("pH_CFU_R2A_plot_colorbinary.pdf", width=3.5, height=2)
```
(note: no TSA is circles and tsa is triangles)

Linear mixed model: 
```{r}
r2a.nectar.glmm <- glmmTMB(pH~ log_total_cfu_r2a +
                             (1|Date/Site), 
                           data= df_culture_limited)
summary(r2a.nectar.glmm)
#p-value: 6.24e-07 ***
# Number of obs: 75, groups:  Site:Date, 4; Date, 4
# Conditional model:
#   Estimate Std. Error z value Pr(>|z|)    
# (Intercept)         5.7689     0.4657  12.387  < 2e-16 ***
#   log_total_cfu_r2a  -4.2670     0.8562  -4.984 6.24e-07 ***
r2a.nectar.dharma <- simulateResiduals(r2a.nectar.glmm)
plot(r2a.nectar.dharma)
```

Conditional model:
                  Estimate Std. Error z value Pr(>|z|)    
(Intercept)         5.9153     0.4175  14.169  < 2e-16 ***
log_total_cfu_r2a  -0.9056     0.1587  -5.707 1.15e-08 ***
 
### Tad's curated data: 

Import Tad's curated dataset: 

This is what Tad says about it: (excel file has generated plot)

*About the CFU vs. pH plot, I looked at the weird data and inspected the whole data. I don't think there is any clear indication that any of the weird data are data entry error or contamination, so we probably shouldn't remove any from the plot. What I did notice is that nectar pH can stay high even when total CFU on R2A is high if colonies on the plate mostly consist of yellow ones. I suspect that the yellow colonies are non-acidifying bacteria, like Pseudomonas, which would be consistent with what Kaoru sees in Leslie and Megan's experimental data. See the plot I made below and attached. Sample size is smaller in this plot, because I did not ask Yadira and Fatou to record yellow vs. non-yellow initially. Along the way, they asked me if I would like them to, as they found yellow to be clearly distinguishable from the rest, and I said sure. I would suggest we use a plot like this, if you agree.* 

This was motivated by a few samples that had low pH but no growth on R2A. We collected culturing data in a lot of different media and wanted to leverage all of that in sharing the results. 

```{r}
df_tad <- read_excel("CFU_pH_curated_by_Tad.xlsx")

#change names: 
names(df_tad)[7] <- "log_total_cfu_r2a"
df_tad$log_total_cfu_r2a <- as.numeric(df_tad$log_total_cfu_r2a)

names(df_tad)[11] <- "pH"

#add metadata that Tad deleted: 

#download complete dataset with sample ID numbers that Tad added
BSURP_2022_Dish_data_all_unedited_data <- read_csv("BSURP 2022 Dish data - all unedited data.csv")

#only take relevant columns: 
sample_metadata <- BSURP_2022_Dish_data_all_unedited_data[,c(1:8)]

#left join to tad dataframe: 
df_tad <- left_join(df_tad, sample_metadata)

#remove zeroes: 
df_tad <- df_tad %>% filter(pH>0)
df_tad <- df_tad %>% filter(coloring!="NA")
df_tad$coloring <- as.factor(df_tad$coloring)
df_tad$pH <- as.numeric(df_tad$pH)
```

Plot: 
```{r}
culture_colors <- c("black"="black", "grey"="grey60", "white"="grey90")

set.seed(4)

pH_CFU_R2A_plot_tad <- ggplot(df_tad, aes(x=log_total_cfu_r2a, y=pH))+
  geom_jitter(aes(color=coloring, alpha=.95), show.legend = F) +
  scale_x_continuous(name ="Bacterial density in nectar (log(total CFU on R2A/uL+1)") +
  geom_abline(intercept = 6.5829, slope = -1.1336) +
  ylim(0,8) +
  scale_color_manual(values = culture_colors)
  
pH_CFU_R2A_plot_tad


ggsave("pH_CFU_R2A_plot_tad.pdf", width=3.5, height=2.2)

```

Linear mixed model: 
```{r}
r2a.tad.nectar.glmm <- glmmTMB(pH ~ log_total_cfu_r2a +
                             (1|Date/Site), 
                           data= df_tad)
summary(r2a.tad.nectar.glmm)
#p-value: 6.24e-07 ***
# Number of obs: 75, groups:  Site:Date, 4; Date, 4
# Conditional model:
#   Estimate Std. Error z value Pr(>|z|)    
# (Intercept)         5.7689     0.4657  12.387  < 2e-16 ***
#   log_total_cfu_r2a  -4.2670     0.8562  -4.984 6.24e-07 ***
r2a.tad.nectar.glmm.dharma <- simulateResiduals(r2a.tad.nectar.glmm)
plot(r2a.tad.nectar.glmm.dharma)
```
 
 Conditional model:
                  Estimate Std. Error z value Pr(>|z|)    
(Intercept)         6.5829     0.6282  10.479  < 2e-16 ***
log_total_cfu_r2a  -1.1336     0.2106  -5.382 7.37e-08 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1