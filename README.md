# n06_nectarmicrobes_ecoevo

This is the gitlab repo for a paper describing the rapid evolution of resistance to priority effects by a nectar yeast species to lowered pH by nectar bacteria. This work was conducted at Stanford University in the lab of Tadashi Fukami. 

## Abstract

Priority effects, where arrival order and initial relative abundance modulate local species interactions, can exert taxonomic, functional, and evolutionary influences on ecological communities by driving them to alternative states. It remains unclear if these wide-ranging consequences of priority effects can be explained systematically by a common underlying factor. Here, we identify such a factor in an empirical system. In a series of field and laboratory studies, we focus on the pH of floral nectar affecting nectar-colonizing microbes and their interactions with plants and pollinators. In a field survey, we found that nectar microbial communities in the hummingbird-pollinated shrub, _Diplacus aurantiacus_, were frequently dominated by either bacteria or yeasts within individual flowers. In laboratory experiments, _Acinetobacter nectaris_, the bacterium most commonly found in nectar, exerted a strongly negative priority effect against _Metschnikowia reukaufii_, the most common nectar-specialist yeast, by reducing nectar pH. This priority effect likely explains the mutually exclusive pattern of dominance found in the field survey. Furthermore, experimental evolution simulating hummingbird-assisted dispersal between flowers over a two-month period revealed that_ M. reukaufii_ could evolve rapidly to improve resistance against the priority effect if constantly exposed to _A. nectaris_-induced pH reduction. We also found that low nectar pH could reduce nectar consumption by hummingbirds, suggesting functional consequences of the pH-driven priority effect for plant reproduction. Taken together, these results show that it is possible to identify an overarching factor that simultaneously governs a diversity of eco-evolutionary consequences of priority effects. 

## Description of analyses

This project was conducted in several parts, the analysis of each of which is included in this repo. 

1. **Field survey**: We report the results of a field survey of D. aurantiacus flowers across a 200-km coastline, showing that microbial communities in the nectar exhibit distribution patterns that are consistent with the existence of two alternative community states, one dominated by nectar bacteria and the other dominated by yeasts. 
- Link to data, analysis, and results: https://gitlab.com/teamnectarmicrobe/n06_nectarmicrobes_ecoevo/-/tree/main/field_survey 

2. **Priority effects experiments (#1)**: We describe findings from laboratory experiments that suggest that inhibitory priority effects between bacteria and yeast are responsible for the alternative states we observed in the field survey and that these priority effects are in large part driven by bacteria-induced reduction in nectar pH. 
- Link to data, analysis, and results: https://gitlab.com/teamnectarmicrobe/n06_nectarmicrobes_ecoevo/-/tree/main/priority_effects 

4. **Priority effects experiments (#2)**: Using experimental evolution of nectar yeast in artificial nectar, we  show that the low pH of nectar can cause rapid evolution of yeast populations, resulting in increased resistance to the pH-driven priority effects. 
- Link to data, analysis, and results: https://gitlab.com/teamnectarmicrobe/n06_nectarmicrobes_ecoevo/-/tree/main/ecoevo_priority_effects

5. **Whole genome resequencing**: We explore genomic differences between evolved yeast strains that are potentially associated with differences in the level of resistance to the priority effects. 
- Link to data, analysis, and results: https://gitlab.com/teamnectarmicrobe/n06_nectarmicrobes_ecoevo/-/tree/main/genomics
- NCBI Sequence Read Archive: BioProject PRJNA825574 

6. **Field experiment**: We report a field experiment showing that the low pH of nectar reduces nectar consumption by flower-visiting hummingbirds, suggesting that pH reduction is the likely explanation for our earlier observations that nectar bacteria reduced pollination and seed set.
- Link to data, analysis, and results: https://gitlab.com/teamnectarmicrobe/n06_nectarmicrobes_ecoevo/-/tree/main/pollinator_field_experiment 

## Authors
- Callie R. Chappell (calliech@stanford.edu)
- Manpreet K. Dhami (DhamiM@landcareresearch.co.nz)
- Mark C. Bitter (mcbitter@stanford.edu)
- Lucas Czech (lczech@carnegiescience.edu)
- Sur Herrera Paredes (surh@stanford.edu)
- Katherine Eritano
- Lexi-Ann Golden
- Veronica Hsu
- Clara Kieschnick
- Nicole Rush (nbradon@stanford.edu)
- Tadashi Fukami (fukamit@stanford.edu)

## Contributions 
Conceptualization, funding acquisition: CRC, MKD, and TF, Fieldwork: MKD, NR, and TF, Laboratory experiments and statistical analysis: all authors, Genomics analysis: CRC, MCB, LC, SHP, and LG. Writing and editing: all authors.

## Acknowledgments
We thank Itzel Arias Del Razo, David Cross, Po-Ju Ke, Carolyn Rice, Nic Romano, and Kaoru Tsuji for assistance with field and laboratory work on the field survey (Fig. 2); Sergio Álvarez-Pérez for his assistance with preliminary priority effects experiments (Fig. 3); Adrianna Garner and Jonathan Hernandez for their assistance with laboratory work with the priority effects microcosm experiments (Fig. 5); Jonathan Barros and Briana Martin-Villa for assistance with additional analysis; the students, teaching assistants, and instructors of the Biology 47 (formerly 44Y) class (Fukami, 2013) in 2016-2018, for assistance with the artificial flower experiments (Fig. 7) and in 2019 for assistance with the experimental evolution experiment, in particular Nona Chiariello, Jess Coyle, Bill Gomez, Trevor Hebert, Daria Hekmat-Scafe, Shyamala Malladi, Jesse Miller, and Griselda Morales; Joo Hee Ahn and David Mosko for inspiring us to investigate nectar pH through their Biology 44Y independent project in 2011; Ivana Cvijovic, Moisés Expósito-Alonso, Grant Kinsler, Lauren O’Connell, Kabir Peay, Dmitri Petrov, Gavin Sherlock, and the members of the community ecology group at Stanford for discussion and comments. 

## Funding
This work was supported by National Science Foundation (DEB 1149600, DEB 1737758), Stanford University’s Terman Fellowship, and donation of sequencing materials from Illumina, Inc. CRC was supported by a National Science Foundation Graduate Research Fellowship (DGE 1656518) and a Stanford Graduate Fellowship. LC was supported by the Carnegie Institution for Science at Stanford, California, USA. MKD was supported by Marsden Fund Grant (MFP-LCR-2002). SHP was supported by the Life Sciences Research Foundation. Undergraduate researchers were supported by programs at Stanford University: KE, LG, and CK were supported by the VPUE Biology Summer Research Program. 

## License
This project is open source and liscenced under a Creative Commons non-commercial liscence until published. 

## Project status
Read pre-print here: [pH as an eco-evolutionary driver of priority effects](https://www.google.com/search?q=pH+as+an+eco-evolutionary+driver+of+priority+effects&oq=pH&aqs=chrome.1.69i60j69i59j69i57j69i59j69i60j69i61j69i60l2.1793j0j4&sourceid=chrome&ie=UTF-8) 

Manuscript is currently submitted. 
