/*
    Genesis - A toolkit for working with phylogenetic data.
    Copyright (C) 2014-2021 Lucas Czech

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Contact:
    Lucas Czech <lczech@carnegiescience.edu>
    Department of Plant Biology, Carnegie Institution For Science
    260 Panama Street, Stanford, CA 94305, USA
*/

#include "genesis/genesis.hpp"

#include <cassert>
#include <iomanip>
#include <fstream>
#include <string>

using namespace genesis;
using namespace genesis::population;
using namespace genesis::utils;

/**
 * @brief Helper script to filter out all positions in the genome where sample 13, which is
 * the ancestral population, does not differ from any of the other 12 samples, which are the
 * derived populations exposed to different treatments. By doing this filtering, we are able to use
 * the existing published reference genome for SNP calling, while downstream still only working
 * with SNPs that actually differ between the derived and ancestral populations. That is, we are
 * not interested in SNPs that exist only with respect to the given reference genome, but where
 * all our 13 samples (ancestral and derived) have the same alleles.
 */
int main( int argc, char** argv )
{
    // Activate logging.
    utils::Logging::log_to_stdout();
    utils::Logging::details.time = true;

    // Get input file name, prepare output file name by turning ".vcf" or ".vcf.gz" into ".csv".
    if( argc != 2 ) {
        throw std::runtime_error( "Expect path to vcf file" );
    }
    std::string const vcf_file = std::string( argv[1] );
    std::string csv_file = vcf_file;
    if( utils::file_extension( csv_file ) == "gz" ) {
        csv_file = utils::file_filename( csv_file );
    }
    if( utils::file_extension( csv_file ) == "vcf" ) {
        csv_file = utils::file_filename( csv_file );
    }
    csv_file += ".csv";

    // User output.
    LOG_INFO << "Processing " << vcf_file;

    // Prepare the iterator over vcf records.
    auto vcf_it = VcfInputIterator( vcf_file );

    // Prepare output file for allele frequencies and write its header line.
    std::ofstream csv_out( csv_file );

    // Get the index of sample 13, so that we can access it for comparison.
    auto const s13_idx = vcf_it.header().get_sample_index( "yeast_13" );

    // Iterate the vcf file, write to csv file.
    size_t cnt = 0;
    size_t hit = 0;
    while( vcf_it ) {

        // Basic filtering: Only SNPs, PASS, need allelic depth field, biallelic SNPs only.
        // Also, skip lines where sample 13 does not have genotype information.
        if(
            // ! vcf_it->is_snp() ||
            // ! vcf_it->pass_filter() ||
            // vcf_it->get_alternatives_count() != 1 ||
            (
                vcf_it->begin_format_genotype().valid_value_count_at( s13_idx ) == 1 &&
                vcf_it->begin_format_genotype().get_value_at(0).is_missing()
            )
        ) {
            ++cnt;
            ++vcf_it;
            continue;
        }

        // Get genotype of sample 13. We add the two diploid alternatives, so that we get
        // values 0,1,2 as possible genotypes. That makes it easy and order-independent to compare.
        auto const gt_13 = vcf_genotype_sum( vcf_it->begin_format_genotype().get_values_at( s13_idx ));
        assert( gt_13 <= 2 );

        // We want to write out (hence, not filter) sites where any of the 12 samples differes
        // from the ancient sample 13. Unless all of them differ in the same way. Then, we don't
        // want to write out that line. So, prepare a check for which of the three possible
        // genotypes have been encountered. We then want to have two or three of the values to be set
        // to true for writing the location.
        bool genotypes[3] = { false, false, false };

        // Iterate all sample genotypes.
        for( auto sample : vcf_it->get_format_genotype()) {

            // We already filtered for biallelic SNPs only. Assert this.
            assert( sample.values_per_sample() == 2 );

            // Skip sample 13.
            if( sample.sample_index() == s13_idx ) {
                continue;
            }

            // Skip samples that have no genotype set. We assert that then it is exactly 2,
            // so that we know what we are skipping.
            if( sample.valid_value_count() == 1 && sample.get_value_at(0).is_missing() ) {
                continue;
            }
            // if( sample.valid_value_count() != 2 ) {
            //     LOG_DBG << vcf_it->get_chromosome() << "\t" << vcf_it->get_position() << "\t" << sample.valid_value_count() << "\t" << sample.sample_name() << "\t" << vcf_genotype_string( sample.get_values() );
            // }
            assert( sample.valid_value_count() == 2 );

            // Get the genotype for the current sample.
            auto const gt = vcf_genotype_sum( sample.get_values() );
            assert( gt <= 2 );

            if( gt != gt_13 ) {
                genotypes[gt] = true;
            }

            // // Compare to sample 13. If they differ, we want to report this position, which we
            // // immediately do, and then are done here, so we can leave the loop.
            // // If none of them differ, the condition never triggers, and we will leave without
            // // having reported this position, which is exactly the filtering we want.
            // if( gt != gt_13 ) {
            //     ++hit;
            //
            //     // Write out the current chrom and position in the format that
            //     // `vcftools --positions <filename>` expects.
            //     csv_out << vcf_it->get_chromosome();
            //     csv_out << "\t" << vcf_it->get_position();
            //     csv_out << "\n";
            //     break;
            // }
        }

        if(
            static_cast<int>( genotypes[0] ) +
            static_cast<int>( genotypes[1] ) +
            static_cast<int>( genotypes[2] )
            > 1
            // Use 0 if we want to just filter for all sites where any of the samples differs from
            // sample 13.
            // Use 1 if we want that extra filter of "not all equal", that is, even if samples
            // differs from sample 13, if they all differ in the same way, still filter it out.
        ) {
            ++hit;

            // Write out the current chrom and position in the format that
            // `vcftools --positions <filename>` expects.
            csv_out << vcf_it->get_chromosome();
            csv_out << "\t" << vcf_it->get_position();
            csv_out << "\n";
        }

        // Move to next record.
        ++cnt;
        ++vcf_it;
    }

    LOG_INFO << "Processed " << cnt << " lines, wrote " << hit << " of those as result.";
    return 0;
}
