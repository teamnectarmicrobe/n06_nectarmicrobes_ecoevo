library(tidyverse)
library(harmonicmeanp)
library(tibbletime)

#' Calculate fisher-like OR with pseudocounts
#'
#' @param geno Genotype matrix must e for diploid organisms with
#' id column
#' @param meta Mut have id and condition column. condition should
#' have only two possible values 
#'
#' @return
#' @export
snp_fisher <- function(geno, meta, fisher_pval = FALSE){
  
  conditions <- unique(meta$condition)
  if(length(conditions) != 2)
    stop("ERROR: there must be two conditions")
  
  res <- geno %>%
    pivot_longer(-id, names_to = "snp", values_to = "ma") %>%
    left_join(meta, by = "id") %>%
    group_by(snp) %>%
    summarise(major_c1 = sum(2 - ma[condition == conditions[1]]),
              minor_c1 = sum(ma[condition == conditions[1]]),
              major_c2 = sum(2 - ma[condition == conditions[2]]),
              minor_c2 = sum(ma[condition == conditions[2]]),
              .groups = 'drop') %>%
    mutate(OR.lap = ((major_c1 + 1) / (minor_c1 + 1)) / ((major_c2 + 1) / (minor_c2 + 1)))
  
  if(fisher_pval){
    res <- res %>%
      pmap_dfr(function(snp, major_c1, minor_c1, major_c2, minor_c2,
                        OR.lap){
        mat <- matrix(c(major_c1, minor_c1, major_c1, minor_c2), ncol = 2)
        f <- fisher.test(mat, alternative = "two.sided")
        
        tibble(snp = snp,
               major_c1 = major_c1,
               minor_c1 = minor_c1,
               major_c2 = major_c2,
               minor_c2 = minor_c2,
               OR.lap = OR.lap,
               # OR = f$estimate,
               fisher_pval = f$p.value)
      })
  }
  
  return(res)
}


#' Calculate enirchment p-values for pairs
#'
#' @param selected_conditions 
#' @param geno 
#' @param meta 
#' @param Perms Results of permutation, must have columns OR.lap,
#' snp, perm, and (depending on minP) fisher_pval
#' @param maxT 
#' @param minP
#'
#' @return
#' @export
#'
#' @examples
test_condition_pair <- function(selected_conditions,
                                geno, meta, Perms,
                                maxT = TRUE,
                                minP = FALSE){
  
  if( minP & !("fisher_pval" %in% colnames(Perms)) ){
    stop("ERROR: column fisher_pval must be in Perms when minP is TRUE",
         call. = TRUE)
  }
  
  # Select data from conditions and get odds ratios
  selected_samples <- meta$id[ meta$condition %in% selected_conditions ] 
  assocs <- snp_fisher(geno = geno %>%
                         dplyr::filter(id %in% selected_samples),
                       meta = meta %>%
                         dplyr::filter(id %in% c(selected_samples)),
                       fisher_pval = minP)
  
  # Get odd ratio two-sided p-value
  P.assoc <- assocs %>%
    select(snp, OR.lap) %>%
    mutate(perm = 0) %>%
    bind_rows(Perms) %>%
    mutate(lOR = abs(log(OR.lap))) %>%
    group_by(snp) %>%
    summarise(P = sum(lOR >= lOR[perm == 0]) / length(perm),
              .groups = 'drop') 
  assocs <- assocs %>%
    left_join(P.assoc, by = "snp") 
  
  
  if(maxT){
    # maxT FDR
    maxOR_null <- Perms %>%
      group_by(perm) %>%
      summarise(maxOR = exp(max(abs(log(OR.lap)))),
                .groups = 'drop') %>%
      select(maxOR) %>% unlist
    
    assocs$fdr_maxT <- assocs$OR.lap %>%
      map_dbl(function(OR.lap, maxOR_null){
        sum(maxOR_null >= exp(abs(log(OR.lap)))) / length(maxOR_null)
      }, maxOR_null = maxOR_null)
  }
  
  if(minP){
    minP_null <- Perms %>%
      group_by(perm) %>%
      summarise(minP = min(fisher_pval),
                .groups = 'drop') %>%
      select(minP) %>% unlist
    assocs$fdr_minP <- assocs$fisher_pval %>%
      map_dbl(function(fisher_pval, minP_null){
        sum(minP_null <= fisher_pval) / length(minP_null)
      }, minP_null = minP_null)
    
  }
  
  # Calculate enrichment of extreme associations
  Enrich_extreme <- NULL
  for(thres in 1:8){
    # thres <- 5
    
    obs_n <- assocs %>%
      mutate(lOR = abs(log(OR.lap))) %>%
      summarise(n = sum(lOR > log(thres)) ) %>%
      unlist
    
    expected_n <- Perms %>%
      split(.$perm) %>%
      map_dbl(function(d, thres){
        sum(abs(log(d$OR.lap)) > log(thres))
      }, thres = thres)
    
    Enrich_extreme <- Enrich_extreme %>%
      bind_rows(tibble(OR.thres = thres,
                       pval = sum(expected_n > obs_n ) / length(expected_n)))
  }
  
  return(list(assocs = assocs, enrich = Enrich_extreme))
}


#' Calculate adjustet harmonic mean p-value
#' 
#' Tests the null hypothesis that none of the p-values is
#' significant even when the p-values are dependent.
#'
#' @param pvals 
#' @param L Total number of tests, i.e. genome-wide number of SNPs for
#' genome-wide significance.
#' 
#' @references 
#' https://cran.r-project.org/web/packages/harmonicmeanp/vignettes/harmonicmeanp.html
#'
#' @return
#' @export
harmonic_p <- function(pvals, L){
  W <- 1/L
  p.R <- p.hmp(p = pvals, w = rep(W, length.out = length(pvals)), L = L)
  w.R <- W * length(pvals)
  p.R/w.R
}


#' Harmonic mean p-value on window of p-values
#'
#' @param dat must have snp column corresponding to snp ID,
#' and P column corresponding to p-value.
#' @param Pos must have snp column corresponding to snp ID,
#' scaffold column corresponding to scaffold ID, and pos column
#' corresponding to position within scaffold.
#' @param w_size 
#'
#' @return
#' @export
#'
#' @examples
window_hmp <- function(dat, Pos, w_size){
  dat <- lph_neutral$assocs
  
  hmp_roll <- rollify(~ harmonic_p(pvals = .x, L = nrow(Pos)),
                      window = w_size)
  min_roll <- rollify(min, window = w_size)
  max_roll <- rollify(max, window = w_size)
  
  dat %>%
    left_join(Pos,
              by = "snp") %>%
    split(.$scaffold) %>%
    map_dfr(function(d, w_size){
      if(nrow(d) < w_size){
        return(NULL)
      }
      d %>%
        arrange(pos) %>%
        select(snp, P, scaffold, pos) %>%
        mutate(p.hmp = hmp_roll(P),
               w_start = min_roll(pos),
               w_end = max_roll(pos)) %>%
        select(scaffold, w_start, w_end, p.hmp) %>%
        filter(!is.na(p.hmp))
    }, w_size = w_size)
}

args <- list(geno = "all.genmap.max.miss.1.recode.vcf.012",
             pos = "all.genmap.max.miss.1.recode.vcf.012.pos",
             nperms = 1000,
             w_size = 50,
             minP = TRUE,
             seed = 871783)

# Create metadata table
meta <- tibble(id = 0:12,
               condition = c("lpH", "AN", "AN", "AN",
                             "ancestral", "lpH", "lpH", "lpH",
                             "neutral", "neutral", "neutral",
                             "neutral", "AN")) %>%
  filter(condition != "ancestral")

# Read genotype data
geno <- read_tsv(args$geno, col_names = F) %>%
  rename(id = "X1") %>%
  filter(id %in% meta$id)

# Permute
set.seed(args$seed)
Perms <- NULL
for(i in 1:args$nperms ){
  # cat(i, "\n")
  cat(i, "=", as.character(Sys.time()), "\n")
  meta_i <- meta %>%
    mutate(condition = sample(condition, replace = FALSE)) %>%
    arrange(condition)
  meta_i <- meta_i[1:8, ]
  geno_i <- geno %>%
    filter(id %in% meta_i$id)
  
  res_i <- snp_fisher(geno = geno_i,
                      meta = meta_i,
                      fisher_pval = args$minP) %>%
    select(snp, OR.lap, fisher_pval) %>%
    mutate(perm = i)
  
  Perms <- Perms %>%
    bind_rows(res_i)
}
save(Perms, file = "Perms.rdat")

########## Perform comparison with real data
# Performing Permutation, maxT, and (possibly) minP
lph_neutral <- test_condition_pair(selected_conditions = c("neutral", "lpH"),
                                   geno = geno,
                                   meta = meta,
                                   Perms = Perms,
                                   minP = args$minP)
write_tsv(lph_neutral$assocs, "assocs_lph_neutral.tsv")
write_tsv(lph_neutral$enrich, "enrich_lph_neutral.tsv")
lph_neutral$assocs %>%
  arrange(fdr_maxT, P)
lph_neutral$assocs %>%
  arrange(fdr_minP, P)
lph_neutral$enrich

an_neutral <- test_condition_pair(selected_conditions = c("neutral", "AN"),
                                  geno = geno,
                                  meta = meta,
                                  Perms = Perms,
                                  minP = args$minP)
write_tsv(lph_neutral$assocs, "assocs_AN_neutral.tsv")
write_tsv(lph_neutral$enrich, "enrich_AN_neutral.tsv")
an_neutral$assocs %>%
  arrange(fdr_maxT, P)
an_neutral$assocs %>%
  arrange(fdr_minP, P)
an_neutral$enrich

an_lph <- test_condition_pair(selected_conditions = c("AN", "lpH"),
                              geno = geno, meta = meta, Perms = Perms,
                              minP = args$minP)
write_tsv(lph_neutral$assocs, "assocs_AN_lph.tsv")
write_tsv(lph_neutral$enrich, "enrich_AN_lph.tsv")
an_lph$assocs %>%
  arrange(fdr_maxT, P)
an_lph$assocs %>%
  arrange(fdr_minP, P)
an_lph$enrich

################ Performing aggregation
# Reading positions
Pos <- read_tsv(args$pos, col_names = c("scaffold", "pos"))
Pos$snp <- paste0("X", 1:nrow(Pos))

### Test null hypothesis that no p-value is significant
# Permutation
harmonic_p(pvals = lph_neutral$assocs$P, L = nrow(Pos))
harmonic_p(pvals = an_neutral$assocs$P, L = nrow(Pos))
harmonic_p(pvals = an_lph$assocs$P, L = nrow(Pos))

# ML fisher p-value
if(args$minP){
  print(harmonic_p(pvals = lph_neutral$assocs$fisher_pval, L = nrow(Pos)))
  print(harmonic_p(pvals = an_neutral$assocs$fisher_pval, L = nrow(Pos)))
  print(harmonic_p(pvals = an_lph$assocs$fisher_pval, L = nrow(Pos)))
}

### Window-based analysis
# Permutation
lph_neutral$w_hmp_perm <- window_hmp(dat = lph_neutral$assocs %>%
                                       select(snp, P),
                                     Pos = Pos,
                                     w_size = args$w_size)
write_tsv(lph_neutral$w_hmp_perm, "w_perm_lph_neutral.tsv")
lph_neutral$w_hmp_perm %>%
  arrange(p.hmp)

an_neutral$w_hmp_perm <- window_hmp(dat = an_neutral$assocs %>%
                                      select(snp, P),
                                    Pos = Pos,
                                    w_size = args$w_size)
write_tsv(an_neutral$w_hmp_perm, "w_perm_an_neutral.tsv")
an_neutral$w_hmp_perm %>%
  arrange(p.hmp)

an_lph$w_hmp_perm <- window_hmp(dat = an_lph$assocs %>%
                                  select(snp, P),
                                Pos = Pos,
                                w_size = args$w_size)
write_tsv(an_lph$w_hmp_perm, "w_perm_an_lph.tsv")
an_lph$w_hmp_perm %>%
  arrange(p.hmp)

# ML fisher pval
if(args$minP){
  lph_neutral$w_hmp_fisher <- window_hmp(dat = lph_neutral$assocs %>%
                                         select(snp, P = fisher_pval),
                                       Pos = Pos,
                                       w_size = args$w_size)
  write_tsv(lph_neutral$w_hmp_fisher, "w_fisher_lph_neutral.tsv")
  lph_neutral$w_hmp_fisher %>%
    arrange(p.hmp) %>%
    print()
  
  an_neutral$w_hmp_fisher <- window_hmp(dat = an_neutral$assocs %>%
                                        select(snp, P = fisher_pval),
                                      Pos = Pos,
                                      w_size = args$w_size)
  write_tsv(an_neutral$w_hmp_fisher, "w_fisher_an_neutral.tsv")
  an_neutral$w_hmp_fisher %>%
    arrange(p.hmp) %>%
    print()
  
  an_lph$w_hmp_fisher <- window_hmp(dat = an_lph$assocs %>%
                                    select(snp, P = fisher_pval),
                                  Pos = Pos,
                                  w_size = args$w_size)
  write_tsv(an_lph$w_hmp_fisher, "w_fisher_an_lph.tsv")
  an_lph$w_hmp_fisher %>%
    arrange(p.hmp) %>%
    print()
}


